FROM python

RUN apt-get update && apt-get install -y build-essential libssl-dev zlib1g-dev libbz2-dev \
  libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
  xz-utils tk-dev libffi-dev liblzma-dev python3-openssl git

RUN curl -s https://api.github.com/repos/tomwright/dasel/releases/latest | grep browser_download_url | grep linux_amd64 | cut -d '"' -f 4 | wget -qi - && mv dasel_linux_amd64 dasel && chmod +x dasel

RUN curl https://pyenv.run | bash

ENV PATH /root/.pyenv/bin:${PATH}

ARG VERSIONS

RUN \
  for PYTHON_VERSION in $VERSIONS; do \
  pyenv install ${PYTHON_VERSION}; \
  /root/.pyenv/versions/${PYTHON_VERSION}/bin/python -m pip install --upgrade pip; \
  /root/.pyenv/versions/${PYTHON_VERSION}/bin/python -m pip freeze --all > requirements.txt; \
  /root/.pyenv/versions/${PYTHON_VERSION}/bin/python -m pip install --upgrade -r requirements.txt; \
  done

## Second stage

FROM python:slim

RUN apt-get update && apt-get install -y curl git

COPY --from=0 /root/.pyenv /root/.pyenv
COPY --from=0 dasel /usr/local/bin/dasel

ARG VERSION_PATHS

ENV PATH /etc/poetry/bin:/root/.pyenv/bin:$VERSION_PATHS$PATH

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/etc/poetry python3 - --no-modify-path

RUN poetry config virtualenvs.create false

COPY pyproject.toml poetry.lock .

RUN poetry update && rm poetry.lock pyproject.toml
